<?php

class BodegaDP

{
    
    var $bodega_Id="";
    var $bodega_Extension="";
	var $bodega_Local="";
    
    function setbodega_Id($bod_id)
    {
        $this->bodega_Id=$bod_id;
    }
    function getbodega_Id()
    {
        return $this->bodega_Id;
    }
    function setbodega_Extension($bod_ex)
    {
        $this->bodega_Extension=$bod_ex;
    }
    function getbodega_Extension()
    {
        return $this->bodega_Extension;
    }
     function setbodega_Local($bod_loc)
    {
        $this->bodega_Local=$bod_loc;
    }
    function getbodega_Local()
    {
        return $this->bodega_Local;
    }
}

?>