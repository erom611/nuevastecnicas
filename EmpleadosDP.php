<?php

class empleados
{
    var $empID = "";
    var $empNombre = "";
    var $empTelefono = "";
    var $empDireccion = "";
	var $empEmail= "";
	var $empCargo = "";
    
    function setID($par_empID)
    {
        $this->id=$par_empID;
    }
    function getID()
    {
        return $this->id;
    }
	function setNombreE($par_empNombre)
    {
        $this->nombre=$par_empNombre;
    }
    function getNombreE()
    {
        return $this->nombre;
    }
    
    function setTelefonoE($par_empTelefono)
    {
        $this->telefono=$par_empTelefono;
    }
    function getTelefonoE()
    {
        return $this->telefono;
    }
    
    function setDireccionE($par_empDireccion)
    {
        $this->direccion=$par_empDireccion;
    }
    function getDireccionE()
    {
        return $this->direccion;
    }
    
    function setEmailE($par_empEmail)
    {
        $this->email=$par_empEmail;
    }
    function getEmailE()
    {
        return $this->email;
    }
	
	function setCargoE($par_empCargo)
	{
		$this->cargo=$par_empCargo;
	}
	function getCargoE()
	{
		return $this->cargo;
	}
}

?>
