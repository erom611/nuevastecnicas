<?php
class cliente
{
    var $cliCedula = "";
    var $cliNombre = "";
    var $cliTelefono = "";
	var $cliEmail= "";
    
    function ClienteDP($cliCedula, $cliNombre, $cliTelefono, $cliEmail) {
        $this->cliCedula = $cliCedula;
        $this->cliNombre = $cliNombre;
        $this->cliTelefono = $cliTelefono;
        $this->cliEmail = $cliEmail;
    }
    
    function getCliCedula() {
        return $this->cliCedula;
    }

    function getCliNombre() {
        return $this->cliNombre;
    }

    function getCliTelefono() {
        return $this->cliTelefono;
    }

    function getCliEmail() {
        return $this->cliEmail;
    }
    
    function setCliCedula($cliCedula) {
        $this->cliCedula = $cliCedula;
    }

    function setCliNombre($cliNombre) {
        $this->cliNombre = $cliNombre;
    }

    function setCliTelefono($cliTelefono) {
        $this->cliTelefono = $cliTelefono;
    }

    function setCliEmail($cliEmail) {
        $this->cliEmail = $cliEmail;
    }
}

?>
