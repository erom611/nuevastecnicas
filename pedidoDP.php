<?php
/**
 *
 */
class Pedido
{
  var $id="";
  var $clienteId="";
  var $pedidoCliente="";
  var $pedidoProducto="";
  var $pedidoCerveza="";

  function setId($par_id)
  {
    $this->id=$par_id;
  }
  function getId()
  {
    return $this->id;
  }

  function setClienteId($par_clienteId)
  {
    $this->clienteId=$par_clienteId;
  }
  function getClienteId()
  {
    return $this->clienteId;
  }

  function setPedidoCliente($par_pedidoCliente)
  {
    $this->pedidoCliente=$par_pedidoCliente;
  }
  function getPedidoCliente()
  {
    return $this->pedidoCliente;
  }

  function setpedidoProducto($par_pedidoProducto)
  {
    $this->pedidoProducto=$par_pedidoProducto;
  }
  function getpedidoProducto()
  {
    return $this->pedidoProducto;
  }

  function setPedidoCerveza($par_pedidoCerveza)
  {
    $this->pedidoCerveza=$par_pedidoCerveza;
  }
  function getPedidoCerveza()
  {
    return $this->pedidoCerveza;
  }
}

 ?>
