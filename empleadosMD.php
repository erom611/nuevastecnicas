<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "portavasos";

// Conexión
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT empNombre, empTelefono, empDireccion, empEmail, empCargo FROM Empleados";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Fila por cada dato
    while($row = $result->fetch_assoc()) {
        echo "Nombre: " . $row["empNombre"]. "Teléfono: " . $row["empTelefono"]. "Dirección: " . $row["empDireccion"]. "Email: " . $row["empEmail"]. "Cargo: " . $row["empCargo"].  "<br>";
    }
} else {
    echo "0 results";
}

$conn->close();
?>
