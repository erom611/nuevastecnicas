/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     19/10/2017 10:25:51                          */
/*==============================================================*/


drop table if exists BODEGA;

drop table if exists CERVEZA;

drop table if exists CLIENTES;

drop table if exists EMPLEADOS;

drop table if exists LOCAL;

drop table if exists PEDIDOS;

drop table if exists PRODUCTOS;

/*==============================================================*/
/* Table: BODEGA                                                */
/*==============================================================*/
create table BODEGA
(
   BODID                int not null auto_increment,
   LOCID                int,
   BODEXTENSION         decimal not null,
   BODLOCAL             char(50) not null,
   primary key (BODID)
);

/*==============================================================*/
/* Table: CERVEZA                                               */
/*==============================================================*/
create table CERVEZA
(
   CERVEZAID            int not null auto_increment,
   PEDIDOID             int,
   BODID                int,
   CERVEZANOMBRE        varchar(40) not null,
   CERVEZAEBC           char(5) not null,
   CERVEZAIBU           char(5) not null,
   CERVEZAGRADOS        char(5) not null,
   primary key (CERVEZAID)
);

/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES
(
   CLIENTEID            int not null auto_increment,
   CLIENTENOMBRE        varchar(60) not null,
   CLIENTETELEFONO      char(10) not null,
   CLIENTEDIRECCION     varchar(100) not null,
   CLIENTECI            char(10) not null,
   CLIENTEEMAIL         varchar(50) not null,
   primary key (CLIENTEID)
);

/*==============================================================*/
/* Table: EMPLEADOS                                             */
/*==============================================================*/
create table EMPLEADOS
(
   EMPID                int not null auto_increment,
   LOCID                int,
   EMPNOMBRE            varchar(50) not null,
   EMPTELEFONO          char(10) not null,
   EMPDIRECCION         varchar(50) not null,
   EMPEMAIL             varchar(60) not null,
   EMPCARGO             char(50) not null,
   primary key (EMPID)
);

/*==============================================================*/
/* Table: LOCAL                                                 */
/*==============================================================*/
create table LOCAL
(
   LOCID                int not null auto_increment,
   LOCDIRECCION         varchar(100) not null,
   LOCTELEFONO          char(10) not null,
   LOCMAIL              varchar(50) not null,
   LOCADMINISTRADOR     varchar(100) not null,
   primary key (LOCID)
);

/*==============================================================*/
/* Table: PEDIDOS                                               */
/*==============================================================*/
create table PEDIDOS
(
   PEDIDOID             int not null auto_increment,
   CLIENTEID            int,
   PEDIDOCLIENTE        varchar(60) not null,
   PEDIDOPRODUCTO       varchar(60) not null,
   PEDIDOCERVEZA        varchar(40) not null,
   primary key (PEDIDOID)
);

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS
(
   PRODUCTOID           int not null auto_increment,
   BODID                int,
   PEDIDOID             int,
   PRODUCTONOMBRE       char(50) not null,
   PRODUCTOCANTIDAD     int not null,
   primary key (PRODUCTOID)
);

alter table BODEGA add constraint FK_TIENE foreign key (LOCID)
      references LOCAL (LOCID) on delete restrict on update restrict;

alter table CERVEZA add constraint FK_ALMACENA1 foreign key (BODID)
      references BODEGA (BODID) on delete restrict on update restrict;

alter table CERVEZA add constraint FK_TIENE3 foreign key (PEDIDOID)
      references PEDIDOS (PEDIDOID) on delete restrict on update restrict;

alter table EMPLEADOS add constraint FK_TIENE4 foreign key (LOCID)
      references LOCAL (LOCID) on delete restrict on update restrict;

alter table PEDIDOS add constraint FK_REALIZAN foreign key (CLIENTEID)
      references CLIENTES (CLIENTEID) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_ALMACENA2 foreign key (BODID)
      references BODEGA (BODID) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_TIENE2 foreign key (PEDIDOID)
      references PEDIDOS (PEDIDOID) on delete restrict on update restrict;

